//
// 6 : 4.25
var ROWS = 34;
var COLS = 48;

var MAX_SCALE = 2;

var CASE_BEGIN = 0;
var CASE_END = 1;
var CASE_MIDDLE = 2;
var CASE_OUTSIDE = -1;

var font;

var tilesText = [];
var tileStatus = [];

var x,y,w,h,p;

var colorStrokes;

var chars = [];

// All the lines
var lines = [];

var indices = [];

var saveImage;

function preload() {
	font = loadFont("assets/Munro.ttf");
}

function setup() {
	createCanvas(1800*MAX_SCALE,1275*MAX_SCALE);
	background(255);
	noFill();
	strokeWeight(1);
	textFont(font);
	textSize(13);
	textAlign(CENTER,CENTER);

	colorStrokes = color(0);

	saveImage = true;

	// set chars
	addCharacter('I',5,6);
	addCharacter('T',9,6);
	addCharacter('P',15,6);
	addCharacter('W',6,13);
	addCharacter('I',10,13);
	addCharacter('N',14,13);
	addCharacter('T',20,13);
	addCharacter('E',26,13);
	addCharacter('R',32,13);
	addCharacter('S',6,20);
	addCharacter('H',12,20);
	addCharacter('O',18,20);
	addCharacter('W',24,20);
	addCharacter('2',6,27);
	addCharacter('0',12,27);
	addCharacter('1',16,27);
	addCharacter('6',20,27);

	// Create the list of pointers
	for(var i=0; i<lines.length; i++){
		indices.push(i);
	}

	// Shuffle them
	do{
		shuffle(indices,true);
		var repeated = false;
		for(var i=0;i<indices.length && !repeated;i++){
			if(indices[i] == i){
				repeated = true;
			}
		}
	}while(repeated);

	for(var i=0; i<lines.length; i++){
		println(indices[i]);
	}



	// Set up tiles
	p = 0;
	var txt = "h";
	for(var r=0; r<ROWS; r++){
		for(var c=0; c<COLS; c++){
			var found = false;
			var foundCol = false;
			var foundRow = false;
			var txt1,txt2,txt3;
			for(var i=0; i<lines.length && !found; i++){
				if(lines[i].isTileInside(c,r)){
					found = true;
					txt = lines[i].tileText(c,r);
				}
				if(lines[i].isBeginInSameRow(r)){
					foundRow = true;
					txt2 =getDistanceTextToAIndex(c,r,i);
				}
				if(lines[i].isBeginInSameCol(c)){
					foundCol = true;
					txt3 =getDistanceTextToAIndex(c,r,i);
				}
			}
			if(!found){
				txt = "h";
				if(foundRow && foundCol){
					txt = random(1)<0.5 ? txt2 : txt3;
				}
				else if(foundRow){
					txt = txt2;
				}
				else if(foundCol){
					txt = txt3;
				}
				else{
					txt = getDistanceTextToAIndex(c,r,int(random(lines.length)));
				}
			}



			tileStatus.push(false);
			tilesText.push(txt);
			p++;
		}
	}






  
}

function draw() {
	background(255);

	// Draw grid
	x = 0;
	y = 0;
	w = width/COLS;
	h = w;
	p = 0;
	for(var r=0; r<ROWS; r++){
		x = 0;
		// Draw horizontal lines
		line(0,y,width,y);
		for(var c=0; c<COLS; c++){

			noStroke();
			fill(colorStrokes);
			text(tilesText[p],x,y,w,h);

			if(tileStatus[p]){
				fill(0,0,0,100);
				rect(x,y,w,h);
			}


			noFill();
			stroke(colorStrokes);
			rect(x,y,w,h);
		//	line(x,0,x,height);


			x+=w;
			// Increase pointer
			p++;	
		}
		y += h;
	}
	stroke(colorStrokes);
	line(0,height,width,height);
	line(width,0,width,height);



	if(saveImage){
		save("Poster.png");
		saveImage = false;
	}

	/*
	var ix = int(mouseX/w);
	var iy = int(mouseY/h);
	noStroke();
	fill(colorStrokes);
	text("col: "+ix+", row: "+iy,mouseX,mouseY);
	*/
  
}

function mouseClicked(){
	// Change the status of the corresponding tile
	var ix = int(mouseX/w);
	var iy = int(mouseY/h);
	var index = ix + iy*COLS;

	tileStatus[index] = !tileStatus[index];

}



// Classes
//


function addCharacter(c,tx,ty){

	switch(c){
		case 'E':
			lines.push(new CharacterLine(tx,ty,4,0,0,0));
			lines.push(new CharacterLine(tx,ty,0,1,0,4));
			lines.push(new CharacterLine(tx,ty,1,4,4,4));
			lines.push(new CharacterLine(tx,ty,1,2,4,2));
		break;
		case 'H':
			lines.push(new CharacterLine(tx,ty,0,0,0,4));
			lines.push(new CharacterLine(tx,ty,1,2,3,2));
			lines.push(new CharacterLine(tx,ty,4,0,4,4));
		break;
		case '1':
		case 'I':
			lines.push(new CharacterLine(tx,ty,2,0,2,4));
		break;
		case 'N':
			lines.push(new CharacterLine(tx,ty,0,4,0,0));
			lines.push(new CharacterLine(tx,ty,1,0,2,0));
			lines.push(new CharacterLine(tx,ty,2,1,2,4));
			lines.push(new CharacterLine(tx,ty,3,4,4,4));
			lines.push(new CharacterLine(tx,ty,4,3,4,0));
		break;
		case 'O':
		case '0':
			lines.push(new CharacterLine(tx,ty,0,0,0,4));
			lines.push(new CharacterLine(tx,ty,1,4,3,4));
			lines.push(new CharacterLine(tx,ty,4,4,4,0));
			lines.push(new CharacterLine(tx,ty,3,0,1,0));
		break;
		case 'P':
			lines.push(new CharacterLine(tx,ty,0,0,0,4));
			lines.push(new CharacterLine(tx,ty,1,0,4,0));
			lines.push(new CharacterLine(tx,ty,4,1,4,2));
			lines.push(new CharacterLine(tx,ty,3,2,1,2));
		break;
		case 'R':
			lines.push(new CharacterLine(tx,ty,0,0,0,4));
			lines.push(new CharacterLine(tx,ty,1,0,4,0));
			lines.push(new CharacterLine(tx,ty,4,1,4,2));
			lines.push(new CharacterLine(tx,ty,3,2,1,2));
			lines.push(new CharacterLine(tx,ty,2,3,2,4));
		break;
		case 'S':
			lines.push(new CharacterLine(tx,ty,4,0,0,0));
			lines.push(new CharacterLine(tx,ty,0,1,0,2));
			lines.push(new CharacterLine(tx,ty,1,2,4,2));
			lines.push(new CharacterLine(tx,ty,4,3,4,4));
			lines.push(new CharacterLine(tx,ty,3,4,0,4));
		break;
		case 'T':
			lines.push(new CharacterLine(tx,ty,0,0,4,0));
			lines.push(new CharacterLine(tx,ty,2,1,2,4));
		break;
		case 'W':
			lines.push(new CharacterLine(tx,ty,0,0,0,4));
			lines.push(new CharacterLine(tx,ty,1,4,4,4));
			lines.push(new CharacterLine(tx,ty,4,3,4,0));
			lines.push(new CharacterLine(tx,ty,2,3,2,0));
		break;
		case '2':
			lines.push(new CharacterLine(tx,ty,0,0,4,0));
			lines.push(new CharacterLine(tx,ty,4,1,4,2));
			lines.push(new CharacterLine(tx,ty,3,2,0,2));
			lines.push(new CharacterLine(tx,ty,0,3,0,4));
			lines.push(new CharacterLine(tx,ty,1,4,4,4));
		break;
		case '6':
			lines.push(new CharacterLine(tx,ty,4,0,0,0));
			lines.push(new CharacterLine(tx,ty,0,1,0,4));
			lines.push(new CharacterLine(tx,ty,1,4,4,4));
			lines.push(new CharacterLine(tx,ty,4,3,4,2));
			lines.push(new CharacterLine(tx,ty,3,2,1,2));
		break;


	}



}

// Line for drawing a character
// Coords are in terms of tiles
// relative to a 5x5 grid
function CharacterLine(tx,ty, x1,y1, x2, y2){
	this.index = lines.length;
	this.position = createVector(tx,ty);
	this.begin = createVector(x1,y1);
	this.begin.add(this.position);
	this.end = createVector(x2,y2);
	this.end.add(this.position);
	this.offset = p5.Vector.sub(this.end,this.begin);

	this.isTileInside = function(tx,ty){
		var point = createVector(tx,ty);

		if(p5.Vector.dist(point,this.begin)==0){
			return true;
		}

		var newoffset = p5.Vector.sub(point,this.begin);


		angle = degrees( p5.Vector.angleBetween(newoffset,this.offset));



		/*
		println(angle);
		println(newoffset.mag());
		println(this.offset.mag());
		*/
		//
		// Is inside if the angle between the two vectors is zero
		// and the magnitude is less than the original magnitude
		return ((angle==0) && newoffset.mag()<=this.offset.mag());

	}

	this.distanceToBegin = function (tx, ty){
		var point = createVector(tx,ty);
		var newoffset = p5.Vector.sub(this.begin,point);

		return newoffset;
	}

	this.isBeginInSameRow = function(ty){
		return ty == this.begin.y;
	}

	this.isBeginInSameCol = function(tx){
		return tx == this.begin.x;
	}

	this.tileText = function(tx,ty){
		var texto = "";


		var point = createVector(tx,ty);
		var newoffset = p5.Vector.sub(this.begin,point);

		var ox = newoffset.x;
		var oy = newoffset.y;

		var side = (ox<0) ? "left" : "right";
		var upordown = (oy<0) ? "up" : "down";

		if(!this.isTileInside(tx,ty)){
			return texto;
		}

		// Begin
		else if(p5.Vector.dist(point,this.begin)==0){
			ox = this.offset.x;
			oy = this.offset.y;
	
			side = (ox<0) ? "left" : "right";
			upordown = (oy<0) ? "up" : "down";
			height = (oy<0) ? "above" : "below";

			texto = "Fill this tile\n";
			if(ox!=0){ // sides
				texto+="& the "+abs(ox)+" at the "+side; 
			}
			else{ // sides
				texto+="& the "+abs(oy)+" "+height; 
			}

		}
		// END
		else if(p5.Vector.dist(point,this.end)==0){
			texto = "Fill this tile &\n";
			texto += getDistanceTextToAIndex(this.end.x,this.end.y,indices[this.index]);

		}
		else{ // Middle
			var num;
			var tex;
			var side = (ox<0) ? "left" : "right";
			var upordown = (oy<0) ? "up" : "down";
			if(ox!=0){
				tex = "to the "+side;
				num = abs(ox);
			}
			else{
				tex = upordown;
				num = abs(oy);
			}
			texto = "Go "+num+" tiles "+tex;

		}

		return texto;


	}



}

function getDistanceTextToAIndex(tx,ty,i){
	var distNew = lines[i].distanceToBegin(tx,ty);
	ox = distNew.x;
	oy = distNew.y;

	side = (ox<0) ? "left" : "right";
	upordown = (oy<0) ? "up" : "down";

	var distanceText = "Go ";
	// No need to move left or right
	if(ox==0){
		distanceText += abs(oy) + " tiles "+upordown;
	}
	// No need to move up or down
	else if(oy==0){
		distanceText += abs(ox) + " tiles to the "+side;
	}
	// Move in both axis
	else{
		distanceText += abs(oy) + " tiles "+upordown + " & "+abs(ox) + " to the "+side;
	}
	return distanceText;

}




